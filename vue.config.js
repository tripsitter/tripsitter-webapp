module.exports = {
  configureWebpack: {
    devServer: {
      host: '0.0.0.0'
    }
  },
  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: true,
    },
  },
};
