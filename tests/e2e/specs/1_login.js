// https://docs.cypress.io/api/introduction/api.html
import 'firebase/auth';

describe('Tripsitter login', () => {
    it("shows the login page when not logged in", () => {
        indexedDB.deleteDatabase('firebaseLocalStorageDb');
        cy.visit("/");
        cy.contains("label", "Email");
        cy.contains("label", "Password");
    });

    it('gives an error when trying to log in with a non-existent email', () => {
        cy.get('#login_username').clear().type('test@accountaaaaaaaaaaaaaaaaaaaa.test').should('have.value', 'test@accountaaaaaaaaaaaaaaaaaaaa.test');
        cy.get('#login_password').clear().type('test').should('have.value', 'test');
        cy.get('.button').click();
        cy.contains('.toaster-error', 'There is no user record corresponding to this identifier.');
    });

    it('gives an error when trying to log in with wrong credentials', () => {
        cy.get('#login_username').clear().type('test@account.test').should('have.value', 'test@account.test');
        cy.get('#login_password').clear().type('test').should('have.value', 'test');
        cy.get('.button').click();
        cy.contains('.toaster-error', 'The password is invalid or the user does not have a password.');
    });

    it('send the user to the dashboard page when logging in with correct credentials ', () => {
        cy.get('#login_username').clear().type('test@account.test').should('have.value', 'test@account.test');
        cy.get('#login_password').clear().type('testpassword').should('have.value', 'testpassword');
        cy.get('.button').click();
        cy.url().should('not.include', '/login');
    });


});
