// https://docs.cypress.io/api/introduction/api.html

describe('Tripsitter login', () => {
    it('Switch to trips tab', () => {
        cy.visitWithMockGeolocation('/');
        cy.get('#trips').click();
        cy.contains('#toggle_add_trip', 'Create Trip');
    });

    it('Show create trip', () => {
        cy.get('#toggle_add_trip').click();
        cy.contains("label#trip_name", 'Trip Name');
        cy.contains("label#trip_description", 'Trip Description');
    });

    it('Gives an error when create trip without right information', () => {
        cy.get('.button.trips__new').click();
        cy.contains('.toaster-error', 'Fill in all information');
    });

    it('Creates trip when all info is filled in', () => {
        cy.get('#trip_name input').clear().type('testTrip').should('have.value', 'testTrip');
        cy.get('#trip_description textarea').clear().type('testTrip description').should('have.value', 'testTrip description');
        cy.get('.select__tag').click();
        cy.get('.option').first().click();
        cy.get('.button.trips__new').click();
        cy.contains('.sidebar_item.trip', 'testTrip')
    });

    it('Goes to trip page when clicking on trip', () => {
        cy.get('.sidebar_item.trip').first().click();
        cy.url().should('include', '/trip');
    });

    it('Should delete', () => {
        cy.get('.navbar__options_circle').click();
        cy.get('.option').contains('Delete Trip').click();
        cy.url().should('not.include', '/trip');
    });
});
