export type localDataOptions =
    'tripsitter__theme' |
    'tripsitter__substanceData' |
    'tripsitter__distanceData' |
    'tripsitter__ratingData' |
    'tripsitter__locale';
