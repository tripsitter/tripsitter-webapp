export enum Substances {
    LSD = 'LSD',
    shrooms = 'shrooms',
    truffels = 'truffels',
    mdma = 'mdma',
    DMT = 'DMT',
    salvia = 'salvia',
    keta = 'ketamine',
    cb = '2cb',
}

export enum Gender {
    female = 'female',
    male = 'male',
    other = 'other',
}

export const DefaultLocaleSettings = {
     defaultTheme: true,
     defaultLocale: 'en',
     defaultDistance: 50,
     defaultRating: 60,
};

export const linkRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;
