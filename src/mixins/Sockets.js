export default {
    sockets: {
        add_sit: function (sit) {
            this.$eventBus.$emit('sit', sit);
        },
        remove_sit: function (sitId) {
            this.$eventBus.$emit('cancel_sit', sitId);
        },
        remove_trip: function (tripId) {
            this.$eventBus.$emit('cancel_trip', tripId);
        },
        add_rating: function (rating) {
            this.$eventBus.$emit(`add_rating${rating.planId}`, rating);
        },
        update_rating: function (rating) {
            this.$eventBus.$emit(`update_rating${rating.id}`, rating.rating);
        },
        delete_rating: function (ratingId) {
            this.$eventBus.$emit(`delete_rating${ratingId}`);
        },
        add_plan: function (plan) {
            this.$eventBus.$emit('add_plan', plan);
            this.$eventBus.$emit(`add_plan${plan.sitId}`, plan);
        },
        remove_plan: function (planId) {
            this.$eventBus.$emit(`remove_plan`, (planId));
        },
        toggle_plan: function (planId) {
            this.$eventBus.$emit(`toggle_plan${planId}`);
        },
    }
}
