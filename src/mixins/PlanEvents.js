import {deletePlan} from '../services/PlanService';

export default {
    mounted() {
        this.$socket.emit(`plan-join`, this.plan.id);
        this.$eventBus.$on(`toggle_plan${this.plan.id}`, this.togglePlan);
        this.$eventBus.$on(`add_rating${this.plan.id}`, this.addRating);
        this.plan.ratings.forEach( (r) => {
            this.$eventBus.$on(`delete_rating${r.id}`, () => {
                this.plan.ratings.splice(this.plan.ratings.findIndex((r2) => r2 === r), 1);
            });
            this.$eventBus.$on(`update_rating${r.id}`, (rating) => {
                r.rating = rating;
            });
        });
    },
    beforeDestroy() {
        this.$socket.emit(`plan-leave`, this.plan.id);
        this.$eventBus.$off(`rate_plan-${this.plan.id}`, this.addRating);
        this.$eventBus.$off(`toggle_plan${this.plan.id}`, this.togglePlan);
    },
    methods: {
        addRating(rating) {
            const addedRating = Object.assign({}, rating);
            this.plan.ratings.push(addedRating);
            this.$eventBus.$on(`delete_rating${addedRating.id}`, () => {
                this.plan.ratings.splice(this.plan.ratings.findIndex((r2) => r2 === addedRating), 1);
            });
            this.$eventBus.$on(`update_rating${addedRating.id}`, (rating) => {
                addedRating.rating = rating;
            });
        },
        deletePlan() {
            this.loading = true;
            deletePlan(this, this.plan, () => {
                this.loading = false;
            })
        },
        togglePlan(){
            this.plan.accepted = !this.plan.accepted;
        },
    }
}
