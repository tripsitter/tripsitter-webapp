import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';
import * as localDataService from './services/LocalDataService';
import * as apiService from './services/ApiService';
import * as geoService from './services/GeoService';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faLock, faEllipsisH, faBars, faCog, faTimes, faCaretDown, faCaretUp, faSearch, faRedoAlt, faAngleDown, faCheck, faPaperPlane, faClock, faTrashAlt, faCheckCircle, faMinusCircle, faPlus, faEllipsisV, faLaughBeam, faFrown, faLink, faAngleDoubleDown, faAngleDoubleUp} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faLock, faEllipsisH, faBars, faCog, faTimes, faCaretDown, faCaretUp, faSearch, faRedoAlt, faAngleDown, faCheck, faPaperPlane, faClock, faTrashAlt, faCheckCircle, faMinusCircle, faPlus, faEllipsisV, faLaughBeam, faFrown, faLink, faAngleDoubleDown, faAngleDoubleUp);

Vue.component('font-awesome-icon', FontAwesomeIcon);

import '../src/css/_general.scss';
// @ts-ignore
import VueSocketIO from 'vue-socket.io';

Vue.prototype.$apiService = apiService;
Vue.prototype.$localDataService = localDataService;
Vue.prototype.$geoService = geoService;
Vue.prototype.$eventBus = new Vue();
Vue.prototype.$toasters = [];

Vue.config.productionTip = false;

import firebase from 'firebase/app';
import 'firebase/auth';
import { firebaseConfig } from '@/private/firebaseConfig';

firebase.initializeApp(firebaseConfig);

let app: any = null;

firebase.auth().onAuthStateChanged( async (user) => {
  if (user) {
    Vue.use(new VueSocketIO({
      connection: process.env.VUE_APP_WEBSOCKETS,
      // @ts-ignore
      // tslint:disable-next-line:max-line-length
      options: {query: {token: await firebase.auth().currentUser.getIdToken()}, rejectUnauthorized : false, secure: true},
    }));
  }
  if (!app) {
    app = new Vue({
      router,
      store,
      i18n,
      render: (h) => h(App),
    }).$mount('#app');
  }
});



