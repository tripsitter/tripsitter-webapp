/* tslint:disable */
import Vue from 'vue';
import VueRouter, {Route} from 'vue-router';
import firebase from 'firebase/app';
import 'firebase/auth';


Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        meta: {
            auth: true,
        },
        children: [
            {
                path: '',
                component: () => import('../views/FindTrip.vue'),
                meta: {
                    auth: true,
                },
            },
            {
                path: '/sit/:id',
                component: () => import('../views/Sit.vue'),
                props: true,
                meta: {
                    auth: true,
                },
            },
            {
                path: '/trip/:id',
                component: () => import('../views/Trip.vue'),
                props: true,
                meta: {
                    auth: true,
                },
            },
            {
                path: '/settings',
                component: () => import('../views/Settings.vue'),
                props: true,
                meta: {
                    auth: true,
                },
            },
        ],
        component: () => import('../views/Home.vue'),
    },
    {
        path: '/login',
        name: 'Login',
        meta: {
            guest: true,
        },
        component: () => import('../views/Login.vue'),
    },
    {
        path: '*',
        component: () => import('../views/Error.vue')
    },

];


// @ts-ignore
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});


if (process.env.NODE_ENV === 'development') {
    router.addRoutes([{
        path: '/test',
        component: () => import('../views/Test.vue')
    },]);
}

router.beforeEach(async (to: Route, from: any, next: Function) => {
    const user = await firebase.auth().currentUser;
    // @ts-ignore
    if (to.matched.some((record: object) => record.meta.auth)) {
        if (!user) {
            return next({
                path: '/login',
            });
        }
    } else {
        // @ts-ignore
        if (to.matched.some((record: object) => record.meta.guest)) {
            if (user) {
                return next({
                    path: '/',
                });
            }
        }
    }
    next();
});

export default router;
