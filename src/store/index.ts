import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    dark: false,
    location: null,
    user: {},
  },
  mutations: {
    setDark(state, dark) {
      state.dark = dark;
    },
    setLocation(state, location) {
      state.location = location;
    },
    setUser(state, user) {
      state.user = user;
    },
    state_updateDisplayname(state, name) {
      // @ts-ignore
      this.state.user.displayName = name;
    },
  },
});
