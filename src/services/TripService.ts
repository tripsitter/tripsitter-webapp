export function deleteTrip(vueApp: any, id: any, next?: () => void) {
    vueApp.$apiService.DeleteTrip(id).then(() => {
        vueApp.$toasters['main'].success('Deleted trip');
        vueApp.$socket.emit(`remove-trip`, id);
        vueApp.$eventBus.$emit('cancel_trip', id);
    }).finally(() => {
        if (next) {
            return next();
        }
    });
}
