export function deleteSit(vueApp: any, sit: any, next?: () => void) {
    vueApp.$apiService.DeleteSit(sit.id).then(() => {
        vueApp.$eventBus.$emit('cancel_sit', sit.id);
        vueApp.$toasters['main'].success('Sit deleted');
        vueApp.$socket.emit('remove-sit', sit);
    }).catch((e: any) => {
        vueApp.$toasters['main'].error('Could not delete sit');
    }).finally(() => {
        if (next) {
            return next();
        }
    });
}
