import firebase from 'firebase/app';
import 'firebase/auth';

const baseApiUrl = process.env.VUE_APP_API;

export const AddTrip = (data: object) => {
    return postRequest('trips', data);
};

export const GetTrips = (page: number) => {
    return getRequest(`trips/all/${page}`);
};

export const GetTrip = (tripId: string) => {
    return getRequest(`trips/${tripId}`);
};

export const UpdateTrip = (trip: object) => {
    return putRequest(`trips`, trip);
};

export const DeleteTrip = (tripId: string) => {
    return deleteRequest(`trips/${tripId}`);
};

export const GetUser = (userId: string) => {
    return getRequest(`users/${userId}`);
};

export const UpdateUser = (user: object) => {
    return putRequest(`users`, user);
};

export const UpdateEmail = () => {
    return putRequest(`users/email`, {});
};

export const UpdateDisplayName = () => {
    return putRequest(`users/displayname`, {});
};

export const GetSits = (page: number) => {
    return getRequest(`sits/all/${page}`);
};

export const AddSit = (tripId: number) => {
    return postRequest(`sits/` + tripId, {});
};

export const DeleteSit = (sitId: number) => {
    return deleteRequest(`sits/` + sitId);
};

export const FindTrips = (page: number, options: object) => {
    return postRequest(`trips/finder/` + page, options);
};

export const GetSitInfo = (sitId: number) => {
    return getRequest(`sits/` + sitId);
};

export const GetChat = (sitId: number, page: number) => {
    return getRequest(`chat/${sitId}/${page}`);
};

export const sendMessage = (sitId: string, message: object) => {
    return postRequest(`chat/${sitId}`, message);
};

export const getTripsSits = (tripId: string) => {
    return getRequest(`sits/all/trip/${tripId}`);
};

export const getTripSit = (tripId: string) => {
    return getRequest(`sits/trip/${tripId}`);
};

export const getSitPlans = (sitId: string) => {
    return getRequest(`plans/sit/${sitId}`);
};

export const addPlan = (sitId: string, date: Date) => {
    return postRequest(`plans/${sitId}`, date);
};

export const getPlans = (page: number) => {
    return getRequest(`plans/${page}`);
};

export const deletePlan = (planId: string) => {
    return deleteRequest(`plans/${planId}`);
};

export const togglePlan = (planId: string) => {
    return putRequest(`plans/${planId}`, {});
};

export const getUserRatingInfo = (userId: string) => {
    return getRequest(`rating/user/${userId}`);
};

export const ratePlan = (planId: number, data: object) => {
    return postRequest(`rating/${planId}`, data);
};

export const updateRating = (rateId: number, data: object) => {
    return putRequest(`rating/${rateId}`, data);
};

export const deleteRating = (rateId: number) => {
    return deleteRequest(`rating/${rateId}`);
};

const getRequest = async (url: string) => {
    // @ts-ignore
    const token = await firebase.auth().currentUser.getIdToken();
    return fetch(`${baseApiUrl}/${url}`, {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': 'true',
        },
    }).then((response) => {
        return (!response.ok ? Promise.reject(response.status) : response.json());
    });
};

const postRequest = async (url: string, data: object) => {
    // @ts-ignore
    const token = await firebase.auth().currentUser.getIdToken();
    return fetch(`${baseApiUrl}/${url}`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then((response) => {

        return (!response.ok ? Promise.reject(response.status) : response.json());
    });
};


const putRequest = async (url: string, data: object) => {
    // @ts-ignore
    const token = await firebase.auth().currentUser.getIdToken();
    return fetch(`${baseApiUrl}/${url}`, {
        method: 'PUT',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': 'true',
        },
        body: JSON.stringify(data),
    }).then((response) => {
        return (!response.ok ? Promise.reject(response.status) : response);
    });
};

const deleteRequest = async (url: string) => {
    // @ts-ignore
    const token = await firebase.auth().currentUser.getIdToken();
    return fetch(`${baseApiUrl}/${url}`, {
        method: 'DELETE',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': 'true',
        },
    }).then((response) => {
        return (!response.ok ? Promise.reject(response.status) : response);
    });
};
