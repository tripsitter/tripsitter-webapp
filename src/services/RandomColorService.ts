// @ts-ignore
import color from 'tinycolor2';

export function getRandomGradient(uid: number, type = 'diagonal') {
    const color1 = color({h: getPRN(uid) % 360, s: 1, l: 0.7});
    const colours = `${color1.toHexString()}, ${color1.spin(45).toHexString()}`;
    switch (type) {
        case 'diagonal':
            return `linear-gradient(to top right, ${colours})`;
        case 'radial':
            return `radial-gradient(circle, ${colours})`;
        case 'horizontal':
            return `linear-gradient(${colours})`;
        case 'vertical':
            return `linear-gradient(to right, ${colours})`;
        default:
            return `linear-gradient(to top right, ${colours})`;
    }
}

function getPRN(id: number): number {
    return id * 32 % 2147483647;
}
