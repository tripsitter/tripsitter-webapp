export function deletePlan(vueApp: any, plan: any, next?: () => void) {
    vueApp.$apiService.deletePlan(plan.id).then(() => {
        vueApp.$eventBus.$emit('remove_plan', plan.id);
        vueApp.$socket.emit('remove-plan', {id: plan.id, sitId: plan.sitId});
    }).catch(() => {
        vueApp.$toasters['main'].error('Could not delete plan');
    }).finally(() => {
        if (next) {
            return next();
        }
    });
}
