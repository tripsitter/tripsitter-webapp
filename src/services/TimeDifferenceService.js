export default function(date, options) {
    let seconds = Math.floor((new Date() - date) / 1000);
    for (let o of getOptions(options)) {
        let interval = Math.floor(seconds / o.secPer);
        if (interval > 1) {
            return `${interval} ${o.name}s`;
        } else if (interval === 1) {
            return `${interval} ${o.name}`;
        }
    }
    return Math.floor(seconds) + " seconds";
}

function getOptions(options) {
    if (!options) {
        options = [
            {
                secPer: 31536000, name: "year",
            },
            {
                secPer: 2592000, name: "month",
            },
            {
                secPer: 86400, name: "day",
            },
            {
                secPer: 3600, name: "hour",
            },
            {
                secPer: 60, name: "min",
            },
        ];
    }
    return options.sort((a, b) => (a.secPer >= b.secPer) ? -1 : 1);
}
