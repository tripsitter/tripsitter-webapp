import {localDataOptions} from '@/Classes/localDataOptions';

export function getLocalData(name: localDataOptions) {
    return new Promise((response, reject) => {
        if (!localStorage[name]) {
            reject();
        }
        const data = JSON.parse(localStorage[name]);
        response(data);
    });
}

export function updateLocalData(name: localDataOptions, data: any) {
    localStorage[name] = JSON.stringify(data);
}
