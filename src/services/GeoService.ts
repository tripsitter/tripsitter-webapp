import store from '../store/index';

export function calculateDistance(lon: number, lat: number) {
    const loc = store.state.location || {latitude: 0, longitude: 0};
    // @ts-ignore
    const dLat = (lat - loc.latitude) * Math.PI / 180;
    const dLon = (lon - loc.longitude) * Math.PI / 180;
    // @ts-ignore
    // tslint:disable-next-line:max-line-length
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(loc.latitude * Math.PI / 180) * Math.cos(lat * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const d = 6371 * (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
    if (d > 1) {
        return Math.round(d) + 'km';
    } else if (d <= 1) {
        return Math.round(d * 1000) + 'm';
    }
    return d;
}

export function getLocation() {
    return new Promise((resolve, reject) => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                resolve(position);
            }, (e) => {
                reject(e.message);
            });
        } else {
            reject('Could not get location');
        }
    });
}

export function hasPermission() {
    return new Promise((resolve) => {
        navigator.permissions.query({
            name: 'geolocation',
        }).then((permission) => {
            return resolve(permission.state === 'granted');
        }).catch(() => {
            return resolve(false);
        });
    });
}
