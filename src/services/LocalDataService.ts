import {getLocalData, updateLocalData} from '@/services/LocalstorageService';
import {Substances, DefaultLocaleSettings} from '@/Classes/DefaultData';

interface Substance {
    name: string;
    selected: boolean;
}

export function getTheme() {
    return new Promise((response) => {
        getLocalData('tripsitter__theme').then((res) => {
             response(res);
        }).catch(() => {
            response(DefaultLocaleSettings.defaultTheme);
        });
    });
}

export function updateTheme(theme: any) {
    updateLocalData('tripsitter__theme', theme);
}

export function getSubstanceData() {
    return new Promise((response) => {
        getLocalData('tripsitter__substanceData').then((res) => {
            // @ts-ignore
            let substances: Substance[] = res;

            substances = substances.filter((s: Substance) => {
                return (s.name in Substances);
            });

            Object.keys(Substances).forEach((s) => {
                if (substances.filter((s2) => s2.name === s).length === 0) {
                    substances.push({name: s, selected: false});
                }
            });
            response(substances);
        }).catch(() => {
            response(Object.keys(Substances).map((s) => {
                return {name: s, selected: true};
            }));
        });
    });
}

export function updateSubstanceSearch(substances: object) {
    updateLocalData('tripsitter__substanceData', substances);
}

export function getDistance() {
    return new Promise((response) => {
        getLocalData('tripsitter__distanceData').then((res) => {
            response(res);
        }).catch(() => {
            response(DefaultLocaleSettings.defaultDistance);
        });
    });
}

export function updateRatingSearch(rating: number) {
    updateLocalData('tripsitter__ratingData', rating);
}

export function getRating() {
    return new Promise((response) => {
        getLocalData('tripsitter__ratingData').then((res) => {
            response(res);
        }).catch(() => {
            response(DefaultLocaleSettings.defaultRating);
        });
    });
}

export function updateDistanceSearch(distance: number) {
    updateLocalData('tripsitter__distanceData', distance);
}

export function getLocale() {
    return new Promise((response) => {
        getLocalData('tripsitter__locale').then((res) => {
            response(res);
        }).catch(() => {
            // should be in a config
            response(DefaultLocaleSettings.defaultLocale);
        });
    });
}

export function updateLocale(distance: number) {
    updateLocalData('tripsitter__locale', distance);
}




